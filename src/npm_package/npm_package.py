from __future__ import annotations
import dataclasses
import json
import tarfile
from os import PathLike as OsPathLike
from tarfile import TarFile
from typing import Any, Callable, Dict, Generator, List, Literal, Optional, Union

import cchardet


PathLike = Union[OsPathLike, str]


def _detect_encoding(content: bytes, default: str = "utf-8") -> str:
    detected_encoding: str = cchardet.detect(content).get("encoding", default)
    return default if detected_encoding is None else detected_encoding


class NpmPackageError(Exception):
    """Represents an error concerning an NpmPackage."""


@dataclasses.dataclass
class NpmFile:
    """
    Represents a file inside an npm package.

    Attributes
    ----------
    path: str
        The relative path to the file within the package.
    content: bytes
        The contents of the file.
    """

    def __init__(self, path: str, content: bytes):
        self.path = path
        self.content = content


@dataclasses.dataclass
class NpmTextFile:
    """
    Represents a text file inside an npm package.

    Attributes
    ----------
    path: str
        The relative path to the file within the package.
    content: str
        The contents of the file.
    """

    def __init__(self, path: str, content: str):
        self.path = path
        self.content = content


class NpmPackage:
    """
    Represents an npm package.

    Attributes
    ----------
    tf: TarFile
        The underlying TarFile instance.
    """

    def __init__(self, tf: TarFile, *, preload_package_json: bool):
        self.tf: TarFile = tf
        self._package_json: Optional[Dict] = None
        if preload_package_json:
            self.package_json

    @classmethod
    def open(cls, path: PathLike, *, preload_package_json: bool = False) -> NpmPackage:
        """
        Create an NpmPackage instance from a path to a tar file containing an npm package.

        Parameters
        ----------
        path: PathLike
            The path to the tar file.
        preload_package_json: bool
            If true, load the package json to memory when instantiating the object.
        """
        tf = tarfile.open(path)
        return cls(tf, preload_package_json=preload_package_json)

    def close(self) -> None:
        """Close the underlying tar file."""
        self.tf.close()

    @property
    def name(self) -> str:
        """The name of the package."""
        return self.package_json.get("name", "")

    @property
    def type(self) -> Literal["commonjs", "module", "unspecified"]:
        """The type of the package, one of "commonjs", "module" or "unspecified"."""
        return self.package_json.get("type", "unspecified")

    @property
    def version(self) -> str:
        """The declared package version."""
        return self.package_json.get("version", "")

    @property
    def dependencies(self) -> Dict[str, str]:
        """The package dependencies and their versions."""
        return self.package_json.get("dependencies", {})

    @property
    def scripts(self) -> Dict[str, str]:
        """The defined scripts."""
        return self.package_json.get("scripts", {})

    @property
    def install_scripts(self) -> Dict[str, str]:
        """The defined install scripts."""
        return {
            k: v for k, v in self.scripts.items() if k in ("install", "preinstall", "postinstall")
        }

    @property
    def package_json(self) -> Dict:
        """The package's package.json file contents."""
        if self._package_json is None:
            if self.tf is None:
                raise NpmPackageError("NpmPackage is not open!")

            package_json_path = None
            min_dir = float("inf")
            for member in self.tf.getmembers():
                if not member.isfile():
                    continue
                filename = member.name
                if filename.endswith("/package.json") or filename == "package.json":
                    dir_level = filename.count("/")
                    if dir_level < min_dir:
                        min_dir = dir_level
                        package_json_path = filename
            if package_json_path is None:
                return {}
            raw_package_json = self.extract_text_file(package_json_path)
            if raw_package_json is None:
                return {}
            try:
                self._package_json = json.loads(raw_package_json)
            except json.decoder.JSONDecodeError:
                return {}
        return self._package_json

    def extract_text_file(self, path: str, encoding: Optional[str] = None) -> Optional[str]:
        """
        Extract a single text file at a given path.

        Parameters
        ----------
        path: str
            The path to the text file to extract.
        encoding: Optional[str]
            The encoding of the file to extract.
            If not given, will try to detect the encoding automatically.

        Returns
        -------
        Optional[str]
            The file contents, if the file exists, else None.
        """
        bytes_content = self.extract_file(path)
        if bytes_content is None:
            return None
        if encoding is None:
            encoding = _detect_encoding(bytes_content)
        return bytes_content.decode(encoding)

    def extract_file(self, path: str) -> Optional[bytes]:
        """
        Extract a single file at a given path.

        Parameters
        ----------
        path: str
            The path to the text file to extract.

        Returns
        -------
        Optional[bytes]
            The file contents, if the file exists, else None.
        """
        try:
            tarinfo = self.tf.getmember(path)
        except KeyError:
            return None
        fp = self.tf.extractfile(tarinfo)
        if fp is None:
            return None
        content = fp.read()
        fp.close()
        return content

    def file_names(self) -> Generator[str, None, None]:
        """
        Create a generator over all the file names of the package.

        Returns
        -------
        Generator[str, None, None]
            A generator that yields the individual file names.
        """
        for member in self.tf.getmembers():
            yield member.name

    def text_files(
        self,
        /,
        file_name_filter: Callable[[str], bool] = lambda _: True,
        encoding: Optional[str] = None,
    ) -> Generator[NpmTextFile, None, None]:
        """
        Create a generator over all the text files in the package.

        Parameters
        ----------
        file_name_filter: Callable[[str], bool]
            A function used to filter out files based on their name.
            Files are excluded if the function returns True.
        encoding: Optional[str]
            The encoding of the files to extract.
            If not given, will try to detect the encoding automatically.

        Returns
        -------
        Generator[NpmTextFile, None, None]
            Yields all files passing the filter.
        """
        for file in self.files(file_name_filter=file_name_filter):
            try:
                if encoding is None:
                    encoding = _detect_encoding(file.content)
                content = file.content.decode(encoding)
            except UnicodeDecodeError:
                continue
            yield NpmTextFile(file.path, content)

    def files(
        self, /, file_name_filter: Callable[[str], bool] = lambda _: True
    ) -> Generator[NpmFile, None, None]:
        """
        Create a generator over all the text files in the package.

        Parameters
        ----------
        file_name_filter: Callable[[str], bool]
            A function used to filter out files based on their name.
            Files are excluded if the function returns True.

        Returns
        -------
        Generator[NpmFile, None, None]
            Yields all files passing the filter.
        """
        for member in self.tf.getmembers():
            if not member.isfile():
                continue
            if not file_name_filter(member.name):
                continue
            fp = self.tf.extractfile(member)
            if fp is None:
                # Should not happen since we know that member.isfile() is True
                continue
            content = fp.read()
            fp.close()
            yield NpmFile(member.name, content)

    def __enter__(self) -> NpmPackage:
        return self

    def __exit__(self, *_args: List[Any]) -> None:
        self.tf.close()
