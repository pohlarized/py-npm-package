# npm-package

This package provides a class `NpmPackage` to work with npm package `.tar.gz` archives.

## Installation

You can install this package from source:

```
git clone https://gitlab.com/pohlarized/py-npm-package
cd py-npm-package
pip install .
```

## Usage

The package itself provides an `open` method that can also be used as a context manager:

```python
import npm_package

with npm_package.open('/path/to/package.tar.gz') as npm:
    # `npm` is now an instance of NpmPackage
    print(npm.package_json)
```

You can also use `npm_package.open()` as a normal function, and then call `.close()` on the resulting `NpmPackage` instance when you're finished.

Proper documentation will be added soonTM, but for now you can just read the source, as it's all documented and rather small.
